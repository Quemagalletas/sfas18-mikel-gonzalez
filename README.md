# What is this project?

This project is my submision for the Search for a star 2018.

# Overview

Supermarket Madness is opening a new center in town, the same day as the black friday! And to celebrate that day, they are opening the inscriptions to their new contest.

Saving madness is their new brand contest, where 2-4 will be fighting to get as many sales as they can.

One item at a time will be presented to the contestants, and they will have to find it and take it to the checkout. They only have 5 minutes to get as many sales as they can.

Once the time is over the participant that has saved the most, will be the winner.

HAPPY BLACK FRIDAY AND GOOD LUCK TO ALL OF YOU!

# Why there is only one commit?

Due to problems with source control I was not able to use it, check the appended documentation in itch.io for more information.

# Developer

### Mikel Gonz�lez Alabau
### http://www.mgonzalezal.com
